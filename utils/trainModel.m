function training_output = trainModel(GEEG, model_options)
% function to configurate HsMM structure model, training settings and
% perform the training.
% ALAND ASTUDILLO FEB 2021
% addpath(genpath('hsmm10Feb2020'));

kcondition = 1;
ksubject = 1;
kblock = 1;
data_struct.cond(kcondition).subj(ksubject).block(kblock).data = GEEG.data_prepared;

%% define HsMM parameters and training options (MVN EMISSION MODEL AND LOG DURATION MODEL)
% model_options.n_states = n_states; % 4; %6;  % put zero if you want to run in a range of states
% model_options.min_states = 7; % Min number of states
% model_options.max_states = 14; % Max number of states
% 
% model_options.max_iter_sim = 4; %10; %20; %N times to repeat the algorithm (the final HSMM will be the one with the max FE)

% model_options.duration_max = 80;  % in matlab points 
% 
% model_options.init_option = 'random'; %'random' or 'kmeans'; % Initialization algorithm
% model_options.n_repetitions = 5;	  % Number of times to initialize (for Kmeans initialization only)
% 
% model_options.tol = 10; % tolerancy
% model_options.max_cyc = 150; % max cycle
% model_options.parallel = 1; % use parallel computing
% 
% %    Cond | Subj | Block
% model_options.se = [1,     1,       1]; % Emission model 
% model_options.st = [1,     1,       1]; % Transition model 
% model_options.si = [1,     1,       1]; % Initial State model 
% model_options.sd = [1,     1,       1]; % Duration model 
% 
% model_options.type_cov = 'full';% could be : 'diag'; %'full';

% FULL COV MATRIX CASE: (NOT USED IN THE CURRENT VERSION FOR PRIOR CALCULATION)
% scale_value = 20;
% model_options.scale_factor = mean(diag((inv(cov(GEEG.data_prepared)))))/GEEG.n_dimensions/scale_value;  % for the FULL cov matrix case

% DIAG COV MATRIX CASE:
% model_options.k_conver = 1000; % with this variable you can play with the convergence
% std_value = 5;
% model_options.mean_prec_data = 1/(mean(std(GEEG.data_prepared)))^2;% /std_value; % precision based on the mean std from the data divided by 5
% model_options.shape_gamma = model_options.mean_prec_data ;% *kconver; %0.001; % for the DIAG cov matrix case
% scale_gamma_value = 1000;
% model_options.scale_gamma = 1/scale_gamma_value/model_options.k_conver; %1000; % for the DIAG cov matrix case

switch model_options.type_cov
    case 'full'
        % EMISSION MODEL: NORMAL DIST WITH FULL COV MATRIX
        % ------------------------------------------------
        %e = emis_model.normal_normal_wishart(GEEG.n_dimensions, model_options.n_states);
        e = emis_model.mar3(GEEG.n_dimensions, model_options.n_states,3);

        % DURATION MODEL: LOG NORMAL
        % ---------------------------
        d = dur_model.lognormal_normal_gamma(1, model_options.n_states);  

        % INITIALIZES THE MODEL
        % ---------------------------
        hsmm_instance = hsmm(GEEG.n_dimensions, model_options.n_states, e, [], [], d);

        % NEW PRIOR DEFINITION
        hsmm_instance.priornoinf(GEEG.data_prepared, model_options.duration_max);

        % Emission model Prior:
        % ---------------------------
%         hsmm_instance.emis_model.prior.prec_wishart{1}.scale = eye(GEEG.n_dimensions) * model_options.scale_factor ; 

    %=================================================================
    case 'diag'
        % EMISSION MODEL: NORMAL DIST WITH DIAG COV MATRIX
        % ------------------------------------------------
        e = emis_model.normal_normal_gamma(GEEG.n_dimensions, model_options.n_states);

        % DURATION MODEL: LOG NORMAL
        % ---------------------------
        d = dur_model.lognormal_normal_gamma(1, model_options.n_states);  

        % INITIALIZES THE MODEL
        % ---------------------------
        hsmm_instance = hsmm(GEEG.n_dimensions, model_options.n_states, e, [], [], d);

        % NEW PRIOR DEFINITION
        hsmm_instance.priornoinf(GEEG.data_prepared, model_options.duration_max);
%         hsmm_instance.emis_model.prior.prec_gamma{1}.scale = ones(1, GEEG.n_dimensions) * model_options.scale_gamma ;
%         hsmm_instance.emis_model.prior.prec_gamma{1}.shape = ones(1, GEEG.n_dimensions) * model_options.shape_gamma ;
end
% clear e d 

%% PERFORM HSMM - TRAIN THE MODEL
tic;
 [out_hsmm, sim_array, hsmm_array, model_struct, model_struct_array] = hsmm_instance.train(...
    data_struct,...
    'dmax', model_options.duration_max,...
    'nrep', model_options.n_repetitions,...
    'initoption', model_options.init_option,...
    'maxitersim', model_options.max_iter_sim,...
    'shareemis', model_options.se,...
    'sharetrans', model_options.st,...
    'sharein', model_options.si,...
    'sharedur', model_options.sd,...
    'minstates', model_options.min_states,...
    'maxstates', model_options.max_states,...
    'tol', model_options.tol,...
    'maxcyc', model_options.max_cyc,...
    'parallel', model_options.parallel);

training_time = toc; % in seconds
disp(['Training Finished took ', num2str(training_time/60), ' min']);

%%
training_output.out_hsmm = out_hsmm;
training_output.sim_array = sim_array;
training_output.hsmm_array = hsmm_array;
training_output.model_struct = model_struct;
training_output.model_struct_array = model_struct_array;
training_output.model_options = model_options;
training_output.hsmm_instance = hsmm_instance;
training_output.training_time = training_time;

