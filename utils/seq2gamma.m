%seq2gamma
function gammaV = seq2gamma(seqV,nStates)
% viterbi or state sequence to gamma matrix ( states x time )
% ALAND ASTUDILLO C - OCT 2019
gammaV = zeros(nStates,size(seqV,2));
for k=1:nStates
    gammaV(k,seqV==k)=1;
end