function tranMat = getTranMat(sequence,nStates)
% function to calculates the transition matrix of a sequence of integers by brute force
% sequence is a integer sequence of K posible values (i.e. 3 4 5 6 1...)
% Aland Astudillo C SEPT 2018
% maxValue = max(sequence);
nPoints = numel(sequence);
ySum = zeros(nStates,1);
probMatrix = zeros(nStates,nStates);
for kseq = 1:nPoints-1
    ySum(sequence(kseq)) = ySum(sequence(kseq)) + 1;
    probMatrix(sequence(kseq),sequence(kseq+1)) = probMatrix(sequence(kseq),sequence(kseq+1)) + 1;
end
probMatrix = bsxfun(@rdivide,probMatrix,ySum); 
probMatrix(isnan(probMatrix)) = 0;
tranMat = probMatrix;
