% For trial selection it asumes all subjects have the same length

% Dopamine Modulation experiment sourcemodel generation
addpath('fieldtrip-20210914')

%% Initialize fieldtrip
ft_defaults

%% Load Global Data

% HMM-DOPAMINE project Headmodel and Electrodes
headmodel = ft_read_headmodel('headmodel_eeg.mat');
load('elec_aligned.mat');
elec_aligned = elec_aligned.elec_aligned;
elec = elec_aligned;
atlas = ft_read_atlas('dkatlas.mat');
load('sourcemodel.mat');

% Load HsMM state sequence
load('output/HsMM_output_MVN_sensor_multisubject_fs_125_f13-16_D2.mat')
stateseq = training_output.out_hsmm.stateseq;
training_fs = GEEG.fs_new;
n_states = training_output.out_hsmm.nstates;
clear training_output GEEG
% %% Prepare sourcemodel
% cfg             = [];
% cfg.elec        = elec;
% cfg.headmodel   = headmodel;
% 
% cfg.warpmni     = 'yes';
% cfg.resolution  = 10;
% cfg.inwardshift = -1;
% sourcemodel     = ft_prepare_sourcemodel(cfg);
%%
% cfg         = [];
% cfg.method  = 'interactive';
% cfg.headshape = headmodel.bnd(1);
% cfg.elec    = elec;
% elec_aligned = ft_electroderealign(cfg);
% % %Rotate:    0    5      -90
% % %scale:     1 0.95   1
% % %translate: 25       0   43

%%
% map source locations onto an anatomical label in an atlas
cfg = [];
cfg.interpmethod = 'nearest';
cfg.parameter = 'tissue';
sourcemodel2 = ft_sourceinterpolate(cfg, atlas, sourcemodel);

% %% visualize the coregistration of electrodes, headmodel, and sourcemodel.
% % create colormap to plot parcels in different color
% nLabels     = length(atlas.tissuelabel);
% colr        = hsv(nLabels); 
% vertexcolor = ones(size(atlas.pos,1), 3);
% for i = 1:length(atlas.tissuelabel)
%     index = find(atlas.tissue==i);
%    if ~isempty(index) 
%       vertexcolor(index,:) = repmat(colr(i,:),  length(index), 1);
%    end   
% end
% 
% % make the headmodel surface transparent
% ft_plot_headmodel(headmodel, 'edgecolor', 'none','facecolor', 'black'); alpha 0.1
% ft_plot_mesh(atlas, 'facecolor', 'brain',  'vertexcolor', vertexcolor, 'facealpha', .5);
% %ft_plot_sens(elec_aligned, 'label', 'yes');
% ft_plot_mesh(sourcemodel, 'vertexcolor', 'm')
% view([0 -90 0])

%% Data files
file_list = dir('data/*.set');
%%
file        = file_list(1,1).name;
folder      = file_list(1,1).folder;
data_path   = append(folder,'/',file);
output_path = 'output/beamformer';
status = mkdir(output_path);
%% Preprocess
cfg     = [];
cfg.dataset      = data_path;
cfg.continuous   = 'yes';
cfg.detrend      = 'yes';
data_eeg = ft_preprocessing(cfg);
data_eeg.elec = elec_aligned;
%% Timelock 
cfg                  = [];
cfg.covariance       = 'yes';
cfg.removemean       = 'yes';
all_data             = ft_timelockanalysis(cfg, data_eeg);
%%    
for i = 2:size(file_list,1)
    file        = file_list(i,1).name;
    folder      = file_list(i,1).folder;
    data_path   = append(folder,'/',file);
    output_path = 'output/beamformer';
    status = mkdir(output_path);

    %% Preprocess
    cfg     = [];
    cfg.dataset      = data_path;
    cfg.continuous   = 'yes';
    cfg.detrend      = 'yes';
    data_eeg = ft_preprocessing(cfg);
    data_eeg.elec = elec_aligned;
    %% Timelock 
    cfg                  = [];
    cfg.covariance       = 'yes';
    %cfg.keeptrials       = 'yes';
    cfg.removemean       = 'yes';
    timelock             = ft_timelockanalysis(cfg, data_eeg);
    %% Append all subjects
    cfg = [];
    all_data = ft_appenddata(cfg, all_data, timelock);
    
end

%% Preprocessing or standarization ???

%% Segment into states

time = linspace(0,(size(stateseq,2)/training_fs)-1,size(stateseq,2));
statemat = zeros([n_states size(stateseq,2)]);
for ii = 1:n_states
    statemat(ii,:) = (stateseq == ii);
end

all_trial = cell2mat(all_data.trial);
all_time  = linspace(0,size(all_trial,2)./all_data.fsample,size(all_trial,2));

states_data = {};
for ii = 1:n_states
    [pks,locs,w,p] = findpeaks(statemat(ii,:),training_fs);
    states_data{ii} = [];
    for iii = 1:size(w,2)
        latency = [locs(iii) locs(iii)+w(iii)];
        indx = find(all_time >= latency(1),1):find(all_time >= latency(2),1);
        states_data{ii} = horzcat(states_data{ii}, all_trial(:,indx));
    end
end

%% Create fieldtrip structure for each state
states_struct = {};
for i = 1:n_states
    states_struct{i} = all_data;
    states_struct{i}.trial  = {states_data{i}};
    states_struct{i}.time   = {linspace(1,size(states_data{i},2)/all_data.fsample,size(states_data{i},2))};
end

    %% filter data
    cfg            = [];
    cfg.bpfilter   = 'yes';
    cfg.bpfilttype = 'firws';
    cfg.bpfreq     = [1 30];
    cfg.dftfilter  = 'yes';
    datafilt       = ft_preprocessing(cfg,states_struct{1});
    
    %% re-reference
    cfg            = [];
    cfg.reref      = 'yes';
    cfg.refchannel = 'all';
    rerefmethod    = 'avg';
    datafilt       = ft_preprocessing(cfg,data_eeg);
    
    %% Timelock 
    cfg                  = [];
    cfg.covariance       = 'yes';
    %cfg.keeptrials       = 'yes';
    cfg.removemean       = 'yes';
    timelock             = ft_timelockanalysis(cfg, datafilt);
    %% Append all subjects
    cfg = [];
    all_data = ft_appenddata(cfg, all_data, timelock);


    %% calculate leadfield
    % Uses OpenMEEG binaries. Remember adding to system path
    % OpenMEEG ver 2.4.9999 does not work. Use previous versions
    cfg                  = [];
    cfg.elec             = data_eeg.elec;  % Electrode distances
    cfg.channel          = timelock.label;
    cfg.sourcemodel.pos = sourcemodel.pos;              % 2002v source points
    cfg.sourcemodel.inside = 1:size(sourcemodel.pos,1); % all source points are inside of the brain
    cfg.headmodel        = headmodel;   % volume conduction headmodel
    lf                   = ft_prepare_leadfield(cfg);
    
    %% create spatial filter using the lcmv beamformer
     % Source structure is 6GB. Unless you have to analyze the sources,
     % save the lf and timelock variables which will allow you to do the
     % source reconstruction.

    cfg                    = [];
    cfg.channel            = lf.label;
    cfg.elec               = elec_aligned;
    cfg.method             = 'lcmv';
    cfg.grid               = lf; % leadfield
    cfg.headmodel          = headmodel; % volume conduction model (headmodel)
    cfg.lcmv.keepfilter    = 'yes';
    cfg.lcmv.fixedori      = 'yes';
    cfg.lcmv.projectnoise  = 'yes';
    cfg.lcmv.weightnorm    = 'nai';
    cfg.lcmv.lambda        = '5%';
    cfg.atlas            = sourcemodel2;
    source               = ft_sourceanalysis(cfg, timelock);    %% Save labeld sources time series

    % Find the index of the valid labels
    indx_valid_labels   = find(~strcmp(string(sourcemodel2.tissuelabel),'Unknown'));
    % Find the index of the valid sources
    atlas_indx          = find(ismember(sourcemodel2.tissue, indx_valid_labels));
    
    % Alocate space
    % Save the source signal
    valid_sources.signal        = source.avg.mom(atlas_indx);
    % Save the label of each source
    valid_sources.label         = sourcemodel2.tissuelabel(sourcemodel2.tissue(atlas_indx)); 
    % Save the labels
    valid_sources.tissuelabel   = string(sourcemodel2.tissuelabel(indx_valid_labels));
    
    %%
    source.avg.mom2 = cell2mat(source.avg.mom);
    
    cfg           = [];
    cfg.parameter = 'tissue';
    cfg.interpmethod = 'nearest';
    dkatlas2  = ft_sourceinterpolate(cfg, atlas, source);

    cfg           = [];
    cfg.parameter = 'mom2';
    sourceint  = ft_sourceinterpolate(cfg,source,dkatlas2);
    sourceint  = ft_sourceparcellate([],sourceint, dkatlas2);
    

   %% Save
    labels = dkatlas2.tissuelabel;
    eeg_rest = sourceint.mom2;
    save([output_path '/' file(1:8) '.mat'], 'eeg_rest', 'labels')
